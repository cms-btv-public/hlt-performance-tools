
# HLT performance tools

A collection of scripts to plot trigger performance on Run 3 from DQM histograms and NanoAOD.
The scripts should be applicable to non-BTV tasks with minor configuration changes.

## Quick setup

```
git clone ssh://git@gitlab.cern.ch:7999/cms-btv-public/hlt-performance-tools.git 
cd hlt-performance-tools
source setup.sh
```
## Structure

The repository is structured as follows

```
├── configs
│   └── ... 
├── plots
│   └── ... 
├── README.md
├── scripts
│   └── open_nano_objects.py
├── setup.sh
├── studies
│   ├── 2307_bpix_study.py
│   └── trigger_review.py
├── utils
│   ├── coffea.py
│   ├── misc.py
│   ├── old.py
│   ├── plotting
│   ├── processors.py
│   ├── selections.py
│   ├── statistics.py
│   └── tnp.py
└── workflows
    ├── compare_inputs.py
    └── efficiency_studies.py
```

### Workflows

This includes all scripts that are useful to plot regular things like inputs, compare inputs, or efficiencies.

All scripts should provied a `-h` flag in order to asses its parameters.

To create a .txt files with all root-files in a certain directory, you can do:

```bash
cd /path/to/targetdir
find ~+ -type f -name "*.root" -printf > filelist.txt
```

#### plot_inputs

This script accepts following argumnets:

```
positional arguments:
  files          Files to process. Either give root files or a .txt with paths to root files.

options:
  -h, --help     show this help message and exit
  --label LABEL  Label for the datasets. Should have the same number of arguments as files
  --isdata       If dataset is data
  --debug        Activate debug settings
  --out OUT      Output directory for plots
```

Example:
```bash
python3 workflows/plot_inputs.py /path/to/root_files.txt --out plots/out_dir_name --label Run3
```

#### compare_inputs

This script accepts following argumnets:
```
positional arguments:
  files                 List of files to process. Either give root files or a .txt with paths to root files.

options:
  -h, --help            show this help message and exit
  --label LABEL [LABEL ...]
                        Label for the datasets. Should have the same number of arguments as files
  --isdata              If first dataset is data
  --debug               Activate debug settings
  --out OUT             Output directory for plots
```

Example:
```bash
python3 workflows/compare_inputs.py /path/to/root_files.txt /path/to/more_root_files.txt --out plots/out_dir_name --label Run3 Phase2 
```



### Studies

This directory is dedicated to diferent studies that are done for certain events like the BPix issues.


## Performance plots from DQM

Histograms from the OfflineDQM can be merged over multiple runs.
The DQM histograms can be downloaded from https://cmsweb.cern.ch/dqm/offline/data/browse/ROOT/OfflineData/Run2022/
For more information regarding the Run condition check [CMS OMS](https://cmsoms.cern.ch/cms/fills/report?cms_fill=8212)

Run `python plot_DQM.py -h` to view the options!


## Trigger performance plots from NanoAOD

Plot the trigger efficiency compared to a reference trigger as function of a variable.
NanoAOD should contain information if trigger fired.

Run `python plot_Nano.py -h` to view the options!


## Examples

### OfflineDQM

- [2022-07-29 STEAM Meeting - HLT DQM and offline performance validation](https://indico.cern.ch/event/1185012/contributions/4979099/attachments/2487407/4271138/2022-07-29%20STEAM%20TriggerDQM.pdf)
- [2022-08-19 STEAM Meeting - HLT DQM and offline performance validation](https://indico.cern.ch/event/1191718/contributions/5009365/attachments/2494891/4284794/2022-08-19%20STEAM%20TriggerDQM.pdf)



### NanoAOD

soon (it's really new ;)

