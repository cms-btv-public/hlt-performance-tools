import numpy as np
import scipy.stats


def getError(k, n, gamma=0.682):
    n[n == 0] = 1e-8
    alpha = 0.5 * (1 - gamma)
    lower = k / n - scipy.stats.beta.ppf(alpha, k, n - k + 1)
    upper = scipy.stats.beta.ppf(1 - alpha, k + 1, n - k) - k / n

    lower[k == 0] = 0
    upper[k == n] = 0

    return [lower, upper]


def varianceRatio(a, A, b, B):
    variance = 1 / a - 1 / A + 1 / b - 1 / B
    return variance


def getErrorRatio(a, A, b, B):
    t = (a / A) / (b / B)
    sigma = np.sqrt(varianceRatio(a, A, b, B))
    lower = t * np.exp(-1 * sigma)
    upper = t * np.exp(+1 * sigma)
    return [lower, upper]


def compute_ratios(hist_a, hist_b):
    r_a = hist_a
    r_b = hist_b
    ratios = r_a / (r_b + 1e-9)

    return ratios
