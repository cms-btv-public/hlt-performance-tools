import awkward as ak
import hist
import json
import numpy as np
from coffea import processor
from coffea.nanoevents.methods import candidate
from coffea.processor.accumulator import (
    column_accumulator,
)
from typing import List, Dict

from configs.variables import deepJet_inputs_selected_branches
from utils.coffea import setup_deepJet_jets
from utils.tnp import TagAndProbe
from utils.selections import ttbar_selection


def setup_coffea_objects(events):
    muons = ak.zip(
        {
            "pt": events.Muon_pt,
            "eta": events.Muon_eta,
            "phi": events.Muon_phi,
            "mass": events.Muon_mass,
        },
        with_name="PtEtaPhiMCandidate",
        behavior=candidate.behavior,
    )

    jets = ak.zip(
        {
            "pt": events.Jet_pt,
            "eta": events.Jet_eta,
            "phi": events.Jet_phi,
            "mass": events.Jet_mass,
            "btag": events.Jet_hltPFDeepFlavourJetTags_probb
            + events.Jet_hltPFDeepFlavourJetTags_probbb
            + events.Jet_hltPFDeepFlavourJetTags_problepb,
        },
        with_name="PtEtaPhiMCandidate",
        behavior=candidate.behavior,
    )
    return jets, muons


class InputProcessor(processor.ProcessorABC):
    def __init__(self, datasets, dataset_config=None, split_flavour=True, skip_keys=None):
        self.datasets = datasets
        self.skip_keys = skip_keys if skip_keys is not None else []
        self.dataset_config = dataset_config
        self.split_flavour = split_flavour
        if dataset_config is None:
            self.dataset_config = {key: {"is_data": False} for key in self.datasets}
        else:
            self.dataset_config = self.dataset_config
        with open("utils/plotting/config.json", "r") as openfile:
            self.plot_config = json.load(openfile)
        self.setup_hists()
        super().__init__()

    def setup_hists(self):
        self.hist_dict = {
            key: hist.Hist.new.StrCat(self.datasets, name="era")
            .StrCat(
                [
                    "Jet_category_all",
                    "Jet_category_light",
                    "Jet_category_C",
                    "Jet_category_B",
                ],
                name="flavour",
            )
            .Regular(
                self.plot_config.get(key, {}).get("bins", 16),
                self.plot_config.get(key, {}).get("min", -1.5),
                self.plot_config.get(key, {}).get("max", +1.5),
                name=f"{key}",
                label=self.plot_config.get(key, {}).get("label", key),
                flow=True,
            )
            .Double()
            for key in deepJet_inputs_selected_branches
            if key not in self.skip_keys
        }

    def process(self, events):
        dataset = events.metadata["dataset"]
        jets = setup_deepJet_jets(events, skip_keys=self.skip_keys)
        return_dict = {}
        """
        fill hists with splitted flavour
        """
        basic_selection = jets.jet_pt > 30
        jets = jets[basic_selection]

        if self.dataset_config.get(dataset, dict()).get("is_data", False):
            for key, hist in self.hist_dict.items():
                if getattr(jets, key, None) is not None:
                    fill_kwargs = {
                        "era": dataset,
                        key: ak.flatten(jets[key], axis=-1).to_numpy(),
                        "flavour": "Jet_category_all",
                    }
                    return_dict[key] = hist.fill(**fill_kwargs)
        else:
            for key, hist in self.hist_dict.items():
                if getattr(jets, key, None) is not None:
                    for flavour in [
                        "Jet_category_all",
                        "Jet_category_B",
                        "Jet_category_C",
                        "Jet_category_light",
                    ]:
                        flavour_mask = jets["category"][flavour] == 1
                        fill_kwargs = {
                            "era": dataset,
                            key: ak.flatten(jets[flavour_mask][key], axis=-1).to_numpy(),
                            "flavour": flavour,
                        }
                        return_dict[key] = hist.fill(**fill_kwargs)

        # return_dict[dataset] = {"n_jets": len(jets)}
        return return_dict

    def postprocess(self, accumulator):
        pass


class BPixProcessor(processor.ProcessorABC):
    def __init__(self, datasets, nbins=16):
        self.nbins = nbins
        self.datasets = datasets
        super().__init__()

    def process(self, events):
        dataset = events.metadata["dataset"]

        jet_count_phi = (
            hist.Hist.new.StrCat(self.datasets, name="era")
            .Regular(self.nbins, -np.pi, np.pi, name="phi", label="Jet $\phi$")
            .Double()
        )
        jet_count_phi_selected = (
            hist.Hist.new.StrCat(self.datasets, name="era")
            .Regular(self.nbins, -np.pi, np.pi, name="phi", label="Jet $\phi$")
            .Double()
        )
        tnp_hist_phi_passed = (
            hist.Hist.new.StrCat(self.datasets, name="era")
            .Regular(self.nbins, -np.pi, np.pi, name="phi", label="Jet $\phi$")
            .Double()
        )
        tnp_hist_phi_failed = (
            hist.Hist.new.StrCat(self.datasets, name="era")
            .Regular(self.nbins, -np.pi, np.pi, name="phi", label="Jet $\phi$")
            .Double()
        )

        jet_mask = (
            (ak.num(events.Jet_pt) >= 1.0)
            & ak.any(events.Jet_pt > 30.0, axis=-1)
            & ak.all(events.Jet_pt < 1000.0, axis=-1)
            & ak.any(np.abs(events.Jet_eta) <= 2.5, axis=-1)
            & (
                ak.count(events.Jet_pt, axis=-1)
                == ak.count(events.Jet_hltPFDeepFlavourJetTags_probb, axis=-1)
            )
            & ak.all(
                events.Jet_hltPFDeepFlavourJetTags_probb
                + events.Jet_hltPFDeepFlavourJetTags_probbb
                + events.Jet_hltPFDeepFlavourJetTags_problepb
                <= 1.0,
                axis=-1,
            )
        )

        events = events[jet_mask]

        jets, muons = setup_coffea_objects(events)
        """
        phi hists
        """
        jet_count_phi.fill(era=dataset, phi=ak.flatten(events.Jet_phi))
        deepflavour_score_mask = jets.btag > 0.4
        jet_count_phi_selected.fill(
            era=dataset, phi=ak.flatten(events.Jet_phi[deepflavour_score_mask[:, 0]])
        )

        tnp_jets = TagAndProbe.close_muon_probe(jets, muons, muon_dr=0.4, jet_dr=1.4, btag=0.3)
        bpix_hole_mask = ~((tnp_jets.tag.eta > -1.6) & (tnp_jets.tag.eta < 0.0)) & ~(
            (tnp_jets.tag.phi > -1.25) & (tnp_jets.tag.eta < -0.75)
        )

        tnp_jets = tnp_jets[bpix_hole_mask]

        tnp_hist_phi_passed.fill(era=dataset, phi=tnp_jets.probe.phi[tnp_jets.probe.btag >= 0.3])
        tnp_hist_phi_failed.fill(era=dataset, phi=tnp_jets.probe.phi)

        ret = {
            "phi": jet_count_phi,
            "phi_selected": jet_count_phi_selected,
            "tnp_passed": tnp_hist_phi_passed,
            "tnp_total": tnp_hist_phi_failed,
        }

        return ret

    def postprocess(self, accumulator):
        pass


class SequenceProcessor(processor.ProcessorABC):
    def __init__(
        self,
        bins: int = 20,
        sequences: List[str] = None,
        selections: Dict = None,
        merge_datasets=None,
    ):
        """

        Args:
            sequences (List[str], optional): Trigger Bit sequences to analyze.
            selections (Dict, optional): Selections to apply. This should be a dictionary of the form {str: callable -> mask}.
        """

        self.bins = bins

        self.sequences = sequences
        self.selections = {}
        self.selections.update(selections)
        if merge_datasets is not None:
            self.merge_datasets = merge_datasets
        else:
            self.merge_datasets = {}

    def create_hists(self, **kwargs):
        hists = (
            hist.Hist.new.StrCat(self.sequences, name="sequence")
            .StrCat(self.selections.keys(), name="selection")
            .Regular(self.bins, 0.0, 1.0, name="n_passing", label="Offline b-tag value")
            .Double()
        )
        return hists

    def process(self, events):
        dataset = events.metadata["dataset"]
        hists = self.create_hists()
        """
        select only events that hate the Trigger objects
        """
        events["selTrigObj"] = events.TrigObj[events.TrigObj.id == 1]
        events = events[ak.num(events.selTrigObj) > 0]
        """
        add different selections
        """
        analysis_selections = processor.PackedSelection()
        for selection_name, selection_callable in self.selections.items():
            analysis_selections.add(selection_name, selection_callable(events))

        # find closest jet
        events["selTrigObj", "Jet"] = events.selTrigObj.nearest(events.Jet)
        events["selTrigObj", "Jet", "dr"] = events.selTrigObj.Jet.delta_r(events.selTrigObj)

        # decode self.sequences into filter
        for i, sequence in enumerate(self.sequences):
            events["selTrigObj", sequence] = (events.selTrigObj.filterBits & 1 << i) > 0

        """
        fill histograms
        """
        for sequence in self.sequences:
            for selection_name in analysis_selections.names:
                selected = events.selTrigObj.Jet.btagDeepFlavB[events["selTrigObj", sequence]][
                    analysis_selections.require(**{selection_name: True})
                ]
                hists.fill(
                    sequence=sequence,
                    selection=selection_name,
                    n_passing=ak.flatten(selected).to_numpy(),
                )
        """
        in case you want to save some arrays, do like demonstrated below:

        trigSeq = {
            sequence: column_accumulator(
                ak.flatten(
                    events.selTrigObj.Jet.btagDeepFlavB[events["selTrigObj", sequence]]
                ).to_numpy()
            )
            for sequence in self.sequences
        }
        """

        ret = {dataset: {}}
        ret[dataset]["entries"] = len(events)
        ret[dataset]["sequence_hist"] = hists

        for merge_set in self.merge_datasets.keys():
            ret[merge_set] = {}
        for merge_set, merge_datasets in self.merge_datasets.items():
            if dataset in merge_datasets:
                ret[merge_set]["entries"] = len(events)
                ret[merge_set]["sequence_hist"] = hists
        # ret[dataset].update(trigSeq)

        return ret

    def postprocess(self, accumulator):
        pass


class ExportProcessor(processor.ProcessorABC):
    def __init__(self, paths=None, sequences=None, PU_file=None):
        """

        Args:
            sequences (List[str], optional): Trigger Bit sequences to analyze.
            selections (Dict, optional): Selections to apply. This should be a dictionary of the form {str: callable -> mask}.
        """
        self.paths = paths
        self.sequences = sequences
        if PU_file is not None:
            """
            PU file is strucured like this:
                - LS number
                - recorded luminosity (/μb)
                - RMS bunch instantaneous luminosity/orbit frequency (/μb/bunch)
                - average bunch instantaneous luminosity/orbit frequency (/μb/bunch)
            see:
            https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData
            """
            try:
                with open(PU_file, "r") as PU_file_json:
                    self.PU_dict = json.load(PU_file_json)
            except FileNotFoundError as e:
                print(
                    "PU_file could not be opened!\nMake sure that you do not try to access Lumi information on the run or LS."
                )
            self.PU_dict = {
                run_number: {block[0]: block[1:] for block in luminosity_blocks}
                for run_number, luminosity_blocks in self.PU_dict.items()
            }
        super().__init__()

    @property
    def get_recorded_luminosity(self):
        return np.vectorize(self._get_recorded_luminosity)

    def _get_recorded_luminosity(self, run, LS):
        try:
            return self.PU_dict[str(run)][LS][0]
        except IndexError as e:
            print(e)
            print(f"IndexError in run: {run} and ls: {LS}")
            return 0.0
        except KeyError as e:
            print(e)
            print(f"KeyError in run: {run} and ls: {LS}")
            return 0.0

    def process(self, events):
        dataset = events.metadata["dataset"]

        # only select events that have the trigger object
        events["selTrigObj"] = events.TrigObj[events.TrigObj.id == 1]
        events = events[ak.num(events.selTrigObj) > 0]

        # # apply ttbar-selection
        ttbar_mask = ttbar_selection(events)
        events = events[ttbar_mask]

        # find closest jet
        # events["selTrigObj", "Jet"] = events.selTrigObj.nearest(events.Jet)
        # events["selTrigObj", "Jet", "dr"] = events.selTrigObj.Jet.delta_r(events.selTrigObj)

        # decode self.sequences into filter
        # for i, sequence in enumerate(self.sequences):
        #     events["selTrigObj", sequence] = (events.selTrigObj.filterBits & 1 << i) > 0
        """
        get b-tag values as well as target/refernce-path values
        """
        ret = {}

        # for sequence in self.sequences:
        #     selected = ak.fill_none(ak.max(events.selTrigObj.Jet.btagDeepFlavB[events["selTrigObj", sequence]], axis=-1), -1)
        #     ret[sequence] = column_accumulator(selected.to_numpy())

        for path in self.paths:
            ret[path] = column_accumulator(getattr(events.HLT, path).to_numpy())

        ret["npvs"] = column_accumulator(events.PV.npvs.to_numpy())
        b_pad = ak.pad_none(events.Jet.btagDeepFlavB, 3, axis=1)
        b_pad_none = ak.fill_none(b_pad, -99.0)
        sorted_b = ak.sort(b_pad_none, axis=-1)
        ret["btags"] = column_accumulator(sorted_b[..., -3:].to_numpy())
        # ret["btags"] = column_accumulator(ak.max(events.Jet.btagDeepFlavB, axis=-1).to_numpy())
        ret["recorded_lumi"] = column_accumulator(
            self.get_recorded_luminosity(events.run, events.luminosityBlock)
        )

        return ret

    def postprocess(self, accumulator):
        pass


class TriggerReviewProcessor(processor.ProcessorABC):
    def __init__(self, paths=None, sequences=None, PU_file=None, merge_datasets=None):
        """

        Args:
            sequences (List[str], optional): Trigger Bit sequences to analyze.
            selections (Dict, optional): Selections to apply. This should be a dictionary of the form {str: callable -> mask}.
        """
        self.paths = paths
        self.sequences = sequences
        if merge_datasets is not None:
            self.merge_datasets = merge_datasets
        else:
            self.merge_datasets = {}

        if PU_file is not None:
            """
            PU file is strucured like this:
                - LS number
                - recorded luminosity (/μb)
                - RMS bunch instantaneous luminosity/orbit frequency (/μb/bunch)
                - average bunch instantaneous luminosity/orbit frequency (/μb/bunch)
            see:
            https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData
            """
            try:
                with open(PU_file, "r") as PU_file_json:
                    self.PU_dict = json.load(PU_file_json)
            except FileNotFoundError as e:
                print(
                    "PU_file could not be opened!\nMake sure that you do not try to access Lumi information on the run or LS."
                )
            self.PU_dict = {
                run_number: {block[0]: block[1:] for block in luminosity_blocks}
                for run_number, luminosity_blocks in self.PU_dict.items()
            }
        super().__init__()

    @property
    def get_recorded_luminosity(self):
        return np.vectorize(self._get_recorded_luminosity)

    def _get_recorded_luminosity(self, run, LS):
        try:
            return self.PU_dict[str(run)][LS][0]
        except IndexError as e:
            print(e)
            print(f"IndexError in run: {run} and ls: {LS}")
            return 0.0
        except KeyError as e:
            print(e)
            print(f"KeyError in run: {run} and ls: {LS}")
            return 0.0

    def process(self, events):
        dataset = events.metadata["dataset"]

        # only select events that have the trigger object
        events["selTrigObj"] = events.TrigObj[events.TrigObj.id == 1]
        events = events[ak.num(events.selTrigObj) > 0]

        # # apply ttbar-selection
        ttbar_mask = ttbar_selection(events)
        events = events[ttbar_mask]

        ret = {dataset: {}}
        for path in self.paths:
            ret[dataset][path] = column_accumulator(getattr(events.HLT, path).to_numpy())

        sorted_b = ak.sort(
            ak.fill_none(ak.pad_none(events.Jet.btagDeepFlavB, 3, axis=1), -99.0), axis=-1
        )

        ret[dataset]["npvs"] = column_accumulator(events.PV.npvs.to_numpy())
        ret[dataset]["btags"] = column_accumulator(sorted_b[..., -3:].to_numpy())
        ret[dataset]["recorded_lumi"] = column_accumulator(
            self.get_recorded_luminosity(events.run, events.luminosityBlock)
        )

        for merge_set in self.merge_datasets.keys():
            ret[merge_set] = {}
        for merge_set, merge_datasets in self.merge_datasets.items():
            if dataset in merge_datasets:
                ret[merge_set]["npvs"] = column_accumulator(events.PV.npvs.to_numpy())
                ret[merge_set]["btags"] = column_accumulator(sorted_b[..., -3:].to_numpy())
                ret[merge_set]["recorded_lumi"] = column_accumulator(
                    self.get_recorded_luminosity(events.run, events.luminosityBlock)
                )
                for path in self.paths:
                    ret[merge_set][path] = column_accumulator(getattr(events.HLT, path).to_numpy())

        return ret

    def postprocess(self, accumulator):
        pass
