import numba
import awkward as ak
import numpy as np
import uproot as ur




@numba.njit
def find_4lep(events_jets, builder):
    """Search for valid jet combinations from an array of events

    valid pairs minimize the total chi2
    """
    for jets in events_jets:

        builder.begin_list()
        njets = len(jets)

        for i0 in range(njets):
            for i1 in range(i0 + 1, njets):
                for i2 in range(njets):
                    for i3 in range(i2 + 1, njets):
                        if len({i0, i1, i2, i3}) < 4:
                            continue
                        builder.begin_tuple(4)
                        builder.index(0).integer(i0)
                        builder.index(1).integer(i1)
                        builder.index(2).integer(i2)
                        builder.index(3).integer(i3)
                        builder.end_tuple()
        builder.end_list()

    return builder

def b2g_selection(events) -> ak.Array:
    """implements the b2g selection
    see
    https://indico.cern.ch/event/1216680/contributions/5133808/attachments/2544401/4381566/TSG_5bjetHLTEfficiency_Jieun.pdf

    returns a mask with the selected events
    """
    jet_mask = (
                    (ak.num(events.Jet) >= 6) \
                    & (ak.sum(events.Jet.pt > 80., axis=-1) >= 3) \
                    & (ak.sum(events.Jet.pt > 130., axis=-1) >= 2) \
                    & (ak.sum(events.Jet.pt > 170., axis=-1) >= 1) \
                    & (ak.sum(events.Jet.pt, axis=-1) > 500.)
                )
    """
    Higgs candidates
    """
    higgs_candidates, higgs_mass, higgs_chi2 = find_chi2_candidate(events.Jet, 121.9, 13.5, n_jets=2)    

    remaining_jets_idx = ak.local_index( events.Jet, axis=1)

    higgs_candidate_mask = (
        (remaining_jets_idx != higgs_candidates["0"]) &
        (remaining_jets_idx != higgs_candidates["1"])
    )

    remaining_jets = events.Jet[higgs_candidate_mask]
    """
    Higgs candidates
    """
    w_candidates, w_mass, w_chi2 = find_chi2_candidate(remaining_jets, 83.8, 10., n_jets=2)    

    remaining_jets_idx = ak.local_index( remaining_jets, axis=1)

    w_candidate_mask = (
        (remaining_jets_idx != w_candidates["0"]) &
        (remaining_jets_idx != w_candidates["1"])
    )
    remaining_jets = remaining_jets[w_candidate_mask]
    """   
    top 
    """
    t_candidates, t_mass, t_chi2 = find_chi2_candidate(remaining_jets, 173.8, 16., n_jets=3)    

    total_chi2 = higgs_chi2 + w_chi2 + t_chi2

    chi2_mask = (total_chi2 < 15)[:, 0] & (higgs_mass > 100)

    b2gmask = jet_mask & ak.any(chi2_mask , axis=-1)

    return ak.fill_none(b2gmask, False).to_numpy()

def find_chi2_candidate(jets, mass, resolution, n_jets=2):
    jet_comb = ak.combinations( jets, n=n_jets )
    jet_comb_idx = ak.argcombinations( jets, n=n_jets)

    multijet_mass = reduce(add, [jet_comb[field] for field in jet_comb.fields]).mass
    multijet_chi2 = compute_chi2(multijet_mass, 121.9, 13.5)
    multijet_chi2_argmin = ak.argmin(multijet_chi2, keepdims=True, axis=-1)
    multijet_candidates = jet_comb_idx[multijet_chi2_argmin][:, 0]

    return multijet_candidates, multijet_mass, multijet_chi2[multijet_chi2_argmin]


def compute_chi2(mass, target_mass, resolution):
    chi2 = (mass - target_mass)**2 / resolution**2
    return chi2