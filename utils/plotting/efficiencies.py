import matplotlib
import matplotlib.pyplot as plt
import mplhep
import numpy as np
from collections.abc import KeysView


def plot_efficiency(
    efficiencies,
    sigma_down,
    sigma_up,
    bin_centers=None,
    labels=None,
    path=None,
    title=None,
    xmin=None,
    xmax=None,
    ymax=1,
    xlabel=None,
    normalize=False,
    lumi="$13.6\,$TeV",
    sort=True,
    **kwargs
):
    fig, plot = plt.subplots(figsize=(10, 10))

    mplhep.cms.label("Preliminary", loc=0, ax=plot, data=True, rlabel=lumi)

    # if sort and isinstance(labels, (list, KeysView)):
    #     labels = list(labels)
    #     sorting = np.argsort(labels)
    #     hists = [hists[s] for s in sorting]
    #     sigmas = [sigmas[s] for s in sorting]
    #     labels = [labels[s] for s in sorting]

    
    plot.errorbar(
        bin_centers, efficiencies, yerr=[sigma_down, sigma_up], marker="o", ls='none', capsize=3
    )

    plot.legend(loc=2)
    plot.set_title(title, y=1.1)
    plot.set_xlabel(xlabel)
    plot.set_xlim(xmin, xmax)
    plot.set_ylabel(
        "Efficiency", verticalalignment="bottom"
    )
    if kwargs.get("log", False):
        plot.set_yscale("log")
    plot.set_ylim(0., ymax)

    fig.tight_layout()
    fig.savefig(path, dpi=300)
    plt.close()

def plot_efficiencies(
    efficiencies,
    sigmas_down,
    sigmas_up,
    bin_centers=None,
    labels=None,
    path=None,
    title=None,
    xmin=None,
    xmax=None,
    ymax=1,
    xlabel=None,
    normalize=False,
    lumi="$13.6\,$TeV",
    sort=True,
    **kwargs
):
    fig, plot = plt.subplots(figsize=(10, 10))

    mplhep.cms.label("Preliminary", loc=0, ax=plot, data=True, rlabel=lumi)

    # if sort and isinstance(labels, (list, KeysView)):
    #     labels = list(labels)
    #     sorting = np.argsort(labels)
    #     hists = [hists[s] for s in sorting]
    #     sigmas = [sigmas[s] for s in sorting]
    #     labels = [labels[s] for s in sorting]

    
    for i, (efficiency, sigma_down, sigma_up, label) in enumerate(zip(efficiencies, sigmas_down, sigmas_up, labels)):
        offset = np.min(np.diff(bin_centers)) / 13 * i
        plot.errorbar(
            bin_centers + offset, efficiency, yerr=[sigma_down, sigma_up], marker="o", ls='none', capsize=3, label=label
        )

    plot.legend(facecolor='white', framealpha=1., loc='best')
    plot.set_title(title, y=1.1)
    plot.set_xlabel(xlabel)
    plot.set_xlim(xmin, xmax)
    plot.set_ylabel(
        "Efficiency", verticalalignment="bottom"
    )
    if kwargs.get("log", False):
        plot.set_yscale("log")
    plot.set_ylim(0., ymax)

    fig.tight_layout()
    fig.savefig(path, dpi=300)
    plt.close()
