import matplotlib
import matplotlib.pyplot as plt
import mplhep
import numpy as np
from matplotlib.ticker import AutoMinorLocator
from utils.statistics import getError, getErrorRatio, compute_ratios
from cycler import cycler

matplotlib.use("Agg")
plt.style.use(mplhep.style.CMS)


def plot_hist(
    hists,
    fname,
    textstr=None,
    xlim=None,
    ylim=None,
    xlabel="x-axis",
    ylabel="y-axis",
    normalize=False,
):
    """_summary_

    Args:
        hists (hist.Hist): Histograms of phi out of coffea processor
        fname (str): Filename to save plots
        textstr (str, optional): Textstring to show in plot.
        ylim (_type_, optional): Y-limits for plot, e.g. (0., 1.)
        xlim (_type_, optional): -limits for plot, e.g. (0., 1.)
        xlabel (str, optional): X-label for x-axis
        ylabel (str, optional): Y-label for y-axis
        normalize (bool, optional): Flag if hist should be nmormalized. Defaults to False.
    """
    fig, ax = plt.subplots(2, 1, gridspec_kw={"height_ratios": [3, 1]})
    fig.subplots_adjust(hspace=0)
    custom_cycler = cycler(color=plt.cm.Set1(np.arange(0, 3, 1))) + cycler(marker=["o", "*", "s"])
    # cycler(linestyle=['-', '--', ':', '-.']) +
    # ax[0].set_prop_cycle(custom_cycler)
    # ax[1].set_prop_cycle(custom_cycler[1:])
    mplhep.cms.label("Preliminary", loc=0, ax=ax[0], data=True, year=2023, rlabel=None)

    bin_edges = hists.axes[1].edges
    # for i, name in enumerate(hists.axes[0]):
    # val = hists.values()[i].copy()
    # yerr = np.sqrt(val)
    # ax[0].errorbar(bin_centers, val, yerr=yerr, capsize=4, linestyle="none", label=name)
    mplhep.histplot(
        hists.values(),
        bin_edges,
        yerr=np.sqrt(hists.values()),
        ax=ax[0],
        density=normalize,
        label=hists.axes[0],
    )

    ax[0].grid(which="both", linestyle="dashed")
    ax[0].legend(title="Datasets")
    ax[0].set_xlim(xlim)
    ax[0].set_ylim(ylim)
    ax[0].set_ylabel(ylabel, fontsize=25)

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle="square", facecolor="whitesmoke", alpha=0.5)
    ax[0].text(
        0.05,
        0.95,
        textstr,
        transform=ax[0].transAxes,
        fontsize=25,
        verticalalignment="top",
        bbox=props,
    )

    for i, name in enumerate(hists.axes[0]):
        if i == 0:
            continue
        else:
            if normalize:
                ratios = compute_ratios(
                    hists.values()[0] / np.sum(hists.values()[0]),
                    hists.values()[i] / np.sum(hists.values()[i]),
                )
                yerr = getErrorRatio(
                    hists.values()[0],
                    np.sum(hists.values()[0]),
                    hists.values()[i],
                    np.sum(hists.values()[i]),
                )
            else:
                ratios = compute_ratios(hists.values()[0], hists.values()[i])
                yerr = getError(hists.values()[0], hists.values()[i])

            yerr = abs(ratios - np.array(yerr))
            ax[1].errorbar(
                hists.axes[1].centers,
                ratios,
                xerr=None,
                yerr=yerr,
                # color="black",
                linestyle="None",
                label=name,
            )
    ax[1].axhline(y=1.0, linestyle="dashed", color="grey", alpha=0.5)
    ax[1].xaxis.set_minor_locator(AutoMinorLocator())
    ax[1].tick_params(which="minor", length=4, color="black")
    ax[1].set_ylabel("$\\frac{{{0}}}{{{1}}}$".format(hists.axes[0][0], "otherr"))
    ax[1].set_xlabel(xlabel, fontsize=25)
    ax[1].set_ylim(0.5, 1.5)
    ax[1].set_yticks([0.75, 1.0, 1.25])
    fig.savefig(fname)


def plot_hist_split_flavour(
    hists,
    fname,
    textstr=None,
    xlim=None,
    ylim=None,
    xlabel="x-axis",
    ylabel="y-axis",
    normalize=False,
    log=False,
):
    """_summary_

    Args:
        hists (hist.Hist): Histograms of phi out of coffea processor
        fname (str): Filename to save plots
        textstr (str, optional): Textstring to show in plot.
        ylim (_type_, optional): Y-limits for plot, e.g. (0., 1.)
        xlim (_type_, optional): -limits for plot, e.g. (0., 1.)
        xlabel (str, optional): X-label for x-axis
        ylabel (str, optional): Y-label for y-axis
        normalize (bool, optional): Flag if hist should be nmormalized. Defaults to False.
    """
    fig, ax = plt.subplots(1, 1)
    fig.subplots_adjust(hspace=0)
    mplheplabel = mplhep.cms.label("Preliminary", loc=0, ax=ax, data=True, year=2023, rlabel=None)

    bin_edges = hists.axes[2].edges
    bin_centers = hists.axes[2].centers
    bin_widths = hists.axes[2].widths
    # for i, name in enumerate(hists.axes[0]):
    # val = hists.values()[i].copy()
    # yerr = np.sqrt(val)
    # ax.errorbar(bin_centers, val, yerr=yerr, capsize=4, linestyle="none", label=name)
    # plot first axis flavour splitted

    mplhep.histplot(
        hists.values()[0][1:],
        bins=bin_edges,
        yerr=np.sqrt(hists.values()[0][1:]),
        ax=ax,
        stack=True,
        histtype="fill",
        color=["cornflowerblue", "forestgreen", "orangered"],
        density=normalize,
        label=list(hists.axes[1])[1:],
    )
    lower = np.sum(hists.values()[0][1:], axis=-2) - np.sqrt(np.sum(hists.values()[0][1:], axis=-2))
    upper = np.sum(hists.values()[0][1:], axis=-2) + np.sqrt(np.sum(hists.values()[0][1:], axis=-2))

    print()
    if normalize:
        lower /= sum(hists.values()[0][1:])
        upper /= sum(hists.values()[0][1:])

    opts = {
        "step": "post",
        "label": "Sum unc.",
        "hatch": "///",
        "facecolor": "black",
        "edgecolor": (0, 0, 0, 0.5),
        "linewidth": 0,
    }
    ax.fill_between(bin_edges[:-1], lower, upper, alpha=0.3, **opts)

    if len(hists.values()) > 1:
        for i, value in enumerate(hists.values()[1:]):
            hist_val = value[0]
            mplhep.histplot(
                hist_val,
                bins=bin_edges,
                yerr=True,
                xerr=True,
                ax=ax,
                stack=False,
                histtype="errorbar",
                color="black",
                density=normalize,
                label=list(hists.axes[0])[i + 1],
            )

    ax.grid(which="both", linestyle="dashed")
    legend = ax.legend(title="Datasets", bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_ylabel(ylabel, fontsize=25)
    if log:
        ax.set_yscale("log")
        if ylim is not None:
            ax.set_ylim(ylim)
        else:
            ax.set_ylim((1e-4, None))

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle=None, facecolor="white", alpha=0.0)
    text = ax.text(
        1.05,
        0.5,
        textstr,
        transform=ax.transAxes,
        fontsize=25,
        verticalalignment="top",
        bbox=props,
    )

    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.tick_params(which="minor", length=4, color="black")
    ax.set_xlabel(xlabel, fontsize=25)

    fig.savefig(fname, bbox_extra_artists=(legend, text, *mplheplabel), bbox_inches="tight")
    plt.close()


def plot_hist_split_flavour_ratio(
    hists,
    fname,
    textstr=None,
    xlim=None,
    ylim=None,
    xlabel="x-axis",
    ylabel="y-axis",
    normalize=False,
    log=False,
):
    """_summary_

    Args:
        hists (hist.Hist): Histograms of phi out of coffea processor
        fname (str): Filename to save plots
        textstr (str, optional): Textstring to show in plot.
        ylim (_type_, optional): Y-limits for plot, e.g. (0., 1.)
        xlim (_type_, optional): -limits for plot, e.g. (0., 1.)
        xlabel (str, optional): X-label for x-axis
        ylabel (str, optional): Y-label for y-axis
        normalize (bool, optional): Flag if hist should be nmormalized. Defaults to False.
    """
    fig, ax = plt.subplots(2, 1, gridspec_kw={"height_ratios": [3, 1]})
    fig.subplots_adjust(hspace=0)
    mplheplabel = mplhep.cms.label(
        "Preliminary", loc=0, ax=ax[0], data=True, year=2023, rlabel=None
    )

    bin_edges = hists.axes[2].edges
    bin_centers = hists.axes[2].centers
    bin_widths = hists.axes[2].widths
    # for i, name in enumerate(hists.axes[0]):
    # val = hists.values()[i].copy()
    # yerr = np.sqrt(val)
    # ax[0].errorbar(bin_centers, val, yerr=yerr, capsize=4, linestyle="none", label=name)
    # plot first axis flavour splitted

    mplhep.histplot(
        hists.values()[0][1:],
        bins=bin_edges,
        yerr=np.sqrt(hists.values()[0][1:]),
        ax=ax[0],
        stack=True,
        histtype="fill",
        color=["cornflowerblue", "forestgreen", "orangered"],
        density=normalize,
        label=list(hists.axes[1])[1:],
    )
    if len(hists.values()) > 1:
        for i, value in enumerate(hists.values()[1:]):
            hist_val = value[0]
            mplhep.histplot(
                hist_val,
                bins=bin_edges,
                yerr=True,
                xerr=True,
                ax=ax[0],
                stack=False,
                histtype="errorbar",
                color="black",
                density=normalize,
                label=list(hists.axes[0])[i + 1],
            )

    ax[0].grid(which="both", linestyle="dashed")
    legend = ax[0].legend(title="Datasets", bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
    ax[0].set_xlim(xlim)
    ax[0].set_ylim(ylim)
    ax[0].set_ylabel(ylabel, fontsize=25)
    if log:
        ax[0].set_yscale("log")
        if ylim is not None:
            ax[0].set_ylim(ylim)
        else:
            ax[0].set_ylim((1e-4, None))

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle=None, facecolor="white", alpha=0.0)
    text = ax[0].text(
        1.05,
        0.5,
        textstr,
        transform=ax[0].transAxes,
        fontsize=25,
        verticalalignment="top",
        bbox=props,
    )

    for i, name in enumerate(hists.axes[0]):
        if i == 0:
            continue
        else:
            if normalize:
                ratios = compute_ratios(
                    hists.values()[i][0] / np.sum(hists.values()[i][0]),
                    hists.values()[0][0] / np.sum(hists.values()[0][0]),
                )
                yerr = getErrorRatio(
                    hists.values()[i][0],
                    np.sum(hists.values()[i][0]),
                    hists.values()[0][0],
                    np.sum(hists.values()[0][0]),
                )
            else:
                ratios = compute_ratios(hists.values()[i][0], hists.values()[0][0])
                yerr = getError(hists.values()[i][0], hists.values()[0][0])

            # compute from lower-upper absolute errors
            yerr = abs(ratios - np.array(yerr))
            ax[1].errorbar(
                bin_centers,
                ratios,
                xerr=bin_widths / 2,
                yerr=yerr,
                capsize=0,
                marker="o",
                markersize=4,
                color="black",
                linestyle="None",
                label=name,
            )
    ax[1].axhline(y=1.0, linestyle="dashed", color="grey", alpha=0.5)
    ax[1].xaxis.set_minor_locator(AutoMinorLocator())
    ax[1].tick_params(which="minor", length=4, color="black")
    ax[1].set_ylabel("$\\frac{{{0}}}{{{1}}}$".format(hists.axes[0][0], "otherr"))
    ax[1].set_xlabel(xlabel, fontsize=25)
    ax[1].set_ylim(0.5, 1.5)
    ax[1].set_yticks([0.75, 1.0, 1.25])
    fig.savefig(fname, bbox_extra_artists=(legend, text, *mplheplabel), bbox_inches="tight")
    plt.close()


def plot_phi(
    phis, fname, textstr=None, ylim=None, xlabel="x-axis", ylabel="y-axis", normalize=False
):
    """_summary_

    Args:
        phis (hist.Hist): Histograms of phi out of coffea processor
        fname (str): Filename to save plots
        textstr (str, optional): Textstring to show in plot.
        ylim (_type_, optional): Y-limits for plot, e.g. (0., 1.)
        xlabel (str, optional): X-label for x-axis
        ylabel (str, optional): Y-label for y-axis
        normalize (bool, optional): Flag if hist should be nmormalized. Defaults to False.
    """
    fig, ax = plt.subplots(2, 1, gridspec_kw={"height_ratios": [3, 1]})
    fig.subplots_adjust(hspace=0)
    custom_cycler = cycler(color=plt.cm.Set1(np.arange(0, 3, 1))) + cycler(marker=["o", "*", "s"])
    # cycler(linestyle=['-', '--', ':', '-.']) +
    ax[0].set_prop_cycle(custom_cycler)
    ax[1].set_prop_cycle(custom_cycler[1:])
    mplhep.cms.label("Preliminary", loc=0, ax=ax[0], data=True, year=2023, rlabel=None)

    bin_centers = phis.axes[1].centers
    for i, name in enumerate(phis.axes[0]):
        val = phis.values()[i].copy()
        yerr = np.sqrt(val)
        if normalize:
            yerr /= np.sum(val)
            val /= np.sum(val)
        ax[0].errorbar(bin_centers, val, yerr=yerr, capsize=4, linestyle="none", label=name)

    ax[0].set_axisbelow(True)
    ax[0].grid(which="both", linestyle="dashed")
    ax[0].legend(title="Eras")
    ax[0].set_xlim(-np.pi, np.pi)
    ax[0].set_ylim(0.04, 0.07)
    ax[0].set_ylabel(ylabel, fontsize=25)

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle="square", facecolor="whitesmoke", alpha=0.5)
    ax[0].text(
        0.05,
        0.95,
        textstr,
        transform=ax[0].transAxes,
        fontsize=25,
        verticalalignment="top",
        bbox=props,
    )

    for i, name in enumerate(phis.axes[0]):
        if i == 0:
            continue
        else:
            if normalize:
                ratios = compute_ratios(
                    phis.values()[0] / np.sum(phis.values()[0]),
                    phis.values()[i] / np.sum(phis.values()[i]),
                )
                yerr = getErrorRatio(
                    phis.values()[0],
                    np.sum(phis.values()[0]),
                    phis.values()[i],
                    np.sum(phis.values()[i]),
                )
            else:
                ratios = compute_ratios(phis.values()[0], phis.values()[i])
                yerr = getError(phis.values()[0], phis.values()[i])
            ax[1].errorbar(
                bin_centers,
                ratios,
                xerr=None,
                yerr=yerr,
                # color="black",
                linestyle="None",
                label=name,
            )
    ax[1].axhline(y=1.0, linestyle="dashed", color="grey", alpha=0.5)
    ax[1].xaxis.set_minor_locator(AutoMinorLocator())
    ax[1].tick_params(which="minor", length=4, color="black")
    ax[1].set_ylabel("$\\frac{{{0}}}{{{1}}}$".format(phis.axes[0][0], "otherr"))
    ax[1].set_xlabel(xlabel, fontsize=25)
    ax[1].set_ylim(0.5, 1.5)
    ax[1].set_yticks([0.75, 1.0, 1.25])
    fig.savefig(fname)


def plot_phi_ratio(
    phis_a,
    phis_b,
    fname,
    textstr=None,
    ylim=None,
    xlabel="x-axis",
    ylabel="y-axis",
    normalize=False,
):
    """_summary_

    Args:
        phis_a (hist.Hist): Histograms of phis numerator
        phis_b (hist.Hist): histograms of phis denominator
        fname (str): Filename to save plots
        textstr (str, optional): Textstring to show in plot.
        ylim (_type_, optional): Y-limits for plot, e.g. (0., 1.)
        xlabel (str, optional): X-label for x-axis
        ylabel (str, optional): Y-label for y-axis
        normalize (bool, optional): Flag if hist should be nmormalized. Defaults to False.
    """
    fig, ax = plt.subplots(2, 1, gridspec_kw={"height_ratios": [3, 1]})
    fig.subplots_adjust(hspace=0)
    custom_cycler = cycler(color=plt.cm.Set1(np.arange(0, 3, 1))) + cycler(marker=["o", "*", "s"])
    # cycler(linestyle=['-', '--', ':', '-.']) +
    ax[0].set_prop_cycle(custom_cycler)
    ax[1].set_prop_cycle(custom_cycler[1:])
    mplhep.cms.label("Preliminary", loc=0, ax=ax[0], data=True, year=2023, rlabel=None)
    # mplhep.cms.lumitext(text='14 TeV', ax=ax[0], fontname=None, fontsize=None)

    bin_centers = phis_a.axes[1].centers
    for i, name in enumerate(phis_a.axes[0]):
        frac = phis_a.values()[i] / phis_b.values()[i]
        lower, upper = getError(phis_a.values()[i], phis_b.values()[i])
        yerr = np.concatenate(
            (np.expand_dims(lower, axis=0), np.expand_dims(upper, axis=0)), axis=0
        )
        if normalize:
            yerr /= np.sum(frac)
            frac /= np.sum(frac)
        ax[0].errorbar(
            bin_centers, frac, yerr=yerr, capsize=4, linestyle="none", label=name, markersize=4.0
        )

    ax[0].set_xlim(-np.pi, np.pi)
    ax[0].set_ylim(ylim)
    # ax[0].set_xlabel(xlabel, fontsize=25)
    ax[0].set_ylabel(ylabel, fontsize=25)
    ax[0].grid(which="both", linestyle="dashed")
    ax[0].legend(title="Eras")
    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle="square", facecolor="whitesmoke", alpha=0.5)
    ax[0].text(
        0.05,
        0.95,
        textstr,
        transform=ax[0].transAxes,
        fontsize=25,
        verticalalignment="top",
        bbox=props,
    )

    for i, name in enumerate(phis_a.axes[0]):
        if i == 0:
            continue
        else:
            ratios = compute_ratios(
                phis_a.values()[0] / phis_b.values()[0], phis_a.values()[i] / phis_b.values()[i]
            )
            yerr = getErrorRatio(
                phis_a.values()[0], phis_b.values()[0], phis_a.values()[i], phis_b.values()[i]
            )
            ax[1].errorbar(
                bin_centers,
                ratios,
                xerr=None,
                yerr=None,
                # color="black",
                linestyle="None",
                label=name,
            )
    ax[1].axhline(y=1.0, linestyle="dashed", color="grey", alpha=0.5)
    ax[1].xaxis.set_minor_locator(AutoMinorLocator())
    ax[1].tick_params(which="minor", length=4, color="black")
    ax[1].set_ylabel("$\\frac{{{0}}}{{{1}}}$".format(phis_a.axes[0][0], "otherr"))
    ax[1].set_xlabel(xlabel, fontsize=25)
    ax[1].set_ylim(0.5, 1.5)
    ax[1].set_yticks([0.75, 1.0, 1.25])
    # fig.tight_layout()
    fig.savefig(fname)
