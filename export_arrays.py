import argparse
import glob
import json
import os
import awkward as ak
import numpy as np
from coffea import processor
from coffea.nanoevents import NanoAODSchema
from processors import ExportProcessor
from plotting import plot_efficiency
from utils import getError

def build_default_fileset(base_path):
    fileset = {}
#     fileset['Run2018D'] = glob.glob(f'{base_path}/MuonEG_Run2018D/*.root')
    fileset['Run2022C'] = glob.glob(f'{base_path}/MuonEG_Run2022C/*.root')
    fileset['Run2022D'] = glob.glob(f'{base_path}/MuonEG_Run2022D/*.root')
    fileset['Run2022E'] = glob.glob(f'{base_path}/MuonEG_Run2022E/*.root')
    fileset['Run2022F'] = glob.glob(f'{base_path}/MuonEG_Run2022F/*.root')
    fileset['Run2022G'] = glob.glob(f'{base_path}/MuonEG_Run2022G/*.root')
    with open("AutoMake_Run3.json","w") as f:
        json.dump(fileset,f)
    return fileset

if __name__ == "__main__":
    """
    Read in args
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true", help="Activate debug settings")
    parser.add_argument("--out", default="plots/out/", help="plot output name")
    parser.add_argument("--fileset", default=None, help="Json to the fileset that you are using")
    parser.add_argument("--filedir", default=None, help="Direction to the fileset that you are using, will generate Json file start with Auto")
    args = parser.parse_args()

    print("Args:")
    print(args)

    os.makedirs(args.out, exist_ok=True)

    print("Reading in sequences.json")
    with open("sequences.json", "r") as sequence_json:
        sequences = json.load(sequence_json)["sequences"]

    if args.fileset is None:
        fileset = build_default_fileset(args.filedir)
    print(f"Reading in fileset: {args.fileset}")
    with open(args.fileset, "r") as fileset_json:
        fileset = json.load(fileset_json)

    kwargs = {}
    if args.debug:
        kwargs.update({"maxchunks": 1})
        for key in fileset.keys():
            fileset[key] = fileset[key][:1]

    iterative_run = processor.Runner(
        executor=processor.FuturesExecutor(compression=None, workers=32), schema=NanoAODSchema, **kwargs
    )
    paths = [
                "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5",
                "PFHT330PT30_QuadPFJet_75_60_45_40",
                "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71",
                "IsoMu24",
                "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5",
                "PFHT400_FivePFJet_100_100_60_30_30",
                "PFHT450_SixPFJet36",
                "PFHT450_SixPFJet36_PFBTagDeepJet_1p59",
                "QuadPFJet70_50_40_30",
                "QuadPFJet70_50_40_30_PFBTagParticleNet_2BTagSum0p65",
    ] 

    out = iterative_run(
        fileset,
        treename="Events",
        processor_instance=ExportProcessor(
            sequences=sequences,
            paths=paths,
            PU_file="configs/pileup_2022_310123.json"
        ),
    )

    matching_sequences_paths = {
                                "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5": "hltBTagPFDeepJet4p5Triple",
                                "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71": "hltBTagPFDeepJet0p71Double8Jets30",
                                "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5": "hltBTagPFDeepJet4p5Double",
                                "PFHT450_SixPFJet36_PFBTagDeepJet_1p59": "hltBTagPFDeepJet1p59Single"
    }

    matching_references_paths = {
                                "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5": ["PFHT330PT30_QuadPFJet_75_60_45_40",3],
                                "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71": ["IsoMu24",2],
                                "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5": ["PFHT400_FivePFJet_100_100_60_30_30",2],
                                "PFHT450_SixPFJet36_PFBTagDeepJet_1p59": ["PFHT450_SixPFJet36",1],
    }

    N_bins = 12 

    for path, values in matching_references_paths.items():
        
        reference = values[0]
        index = values[1]
        mask_path = out[path].value
        mask_reference = out[reference].value

        try:
            seq = matching_sequences_paths[ path ]
        except KeyError:
            continue
        print(f"Path:\t{path}")
        print(f"Reference:\t{reference}")
        print(f"Seq:\t{seq}")

        
        # b_tag_values = np.array(out["btags"].value)
        npvs = np.array(out["npvs"].value)
        rec_lumi = np.array(out["recorded_lumi"].value)
        n_passing = mask_path & mask_reference
        n_total = mask_reference
        
        # b_tag_values = np.array(out[seq].value)
        b_tag_values = np.array(out["btags"].value)[..., -index]
            
        # b_tag_values = np.array(out["btags_3rd"].value)
        # b_tag_values2 = np.array(out["btags_2nd"].value)
        # b_tag_values1 = np.array(out["btags_1st"].value)

        # substitute events where trigger did not fire with offline value
        # b_tag_values[np.where(b_tag_value < 0.)] = out["btags"].value[np.where(b_tag_values < 0.)]


        """
        """
        # b_tag values are 0 when there is no sequence present
        sanity_mask = (b_tag_values >= 0.) & np.invert(np.isinf(b_tag_values)) & np.invert( np.isnan(b_tag_values) ) 

        b_tag_values = b_tag_values[sanity_mask]
        n_passing = n_passing[sanity_mask]
        n_total = n_total[sanity_mask]
        npvs = npvs[sanity_mask]
        rec_lumi = rec_lumi[sanity_mask]
        avg_inst_lumi = rec_lumi / 23.31

        """
        plot in b-tag bins
        """
        bins = np.linspace(0, 1., num=N_bins-1, endpoint=False)
        bin_digits = np.digitize(b_tag_values, bins)

        numerator = np.array([np.sum(n_passing[np.where(bin_digits == i)]) for i in range(N_bins)])
        denominator = np.array([np.sum(n_total[np.where(bin_digits == i)]) for i in range(N_bins)])

        efficiency = numerator/denominator
        lower_error, upper_error = getError(numerator, denominator)

        bin_centers = np.diff(np.concatenate((bins, [1.])))/2 + bins
        plot_title = path[:len(path) //2 ] + "\n" + path[len(path) //2 :]
        ymax = np.max(efficiency[1:]) * 1.2
        plot_efficiency(efficiency[1:], lower_error[1:], upper_error[1:], bin_centers, title=plot_title, path=os.path.join(args.out, f"eff_b_tag_{seq}.png"), xlabel="offline {} b-tag value".format(index),ymax=ymax)
        plot_efficiency(efficiency[1:], lower_error[1:], upper_error[1:], bin_centers, title=plot_title, path=os.path.join(args.out, f"eff_b_tag_{seq}_log.png"), xlabel="offline {} b-tag value".format(index), log=True,ymax=ymax)
        """
        plot in npvs 
        """
        bins = np.arange(10, 70, 3)
        bin_digits = np.digitize(npvs, bins)

        numerator = np.array([np.sum(n_passing[np.where(bin_digits == i)]) for i in range(len(bins))])
        denominator = np.array([np.sum(n_total[np.where(bin_digits == i)]) for i in range(len(bins))])

        efficiency = numerator / denominator
        lower_error, upper_error = getError(numerator, denominator)

        ymax = np.min((1., np.max(efficiency[np.isfinite(efficiency)]) * 1.2))
        # ymax = np.max(efficiency[1:]) * 1.2
        # if np.isnan(ymax) or np.isinf(ymax):
        #     ymax = 1.

        bin_centers = np.diff(np.concatenate((bins, [bins[-1]+(bins[-1]-bins[-2])])))/2 + bins
        plot_efficiency(efficiency, lower_error, upper_error, bin_centers, title=plot_title, path=os.path.join(args.out, f"eff_npv_{path}.png"), xlabel="NPV",ymax=ymax)
        plot_efficiency(efficiency, lower_error, upper_error, bin_centers, title=plot_title, path=os.path.join(args.out, f"eff_npv_{path}_log.png"), xlabel="NPV", log=True,ymax=ymax)
        """
        plot in rec lumi 
        """
        bins = np.linspace(np.min(avg_inst_lumi), np.max(avg_inst_lumi), 20)
        bins = np.concatenate( (np.array([-1.]), bins))
        bin_digits = np.digitize(avg_inst_lumi, bins)

        numerator = np.array([np.sum(n_passing[np.where(bin_digits == i)]) for i in range(len(bins))])
        denominator = np.array([np.sum(n_total[np.where(bin_digits == i)]) for i in range(len(bins))])

        efficiency = numerator / denominator
        lower_error, upper_error = getError(numerator, denominator)

        # ymax = np.max(efficiency[np.isfinite(efficiency)]) * 1.2

        plot_title = path[:len(path) //2 ] + "\n" + path[len(path) //2 :]
        bin_centers = np.diff(np.concatenate((bins, [bins[-1]+(bins[-1]-bins[-2])])))/2 + bins
        bin_centers[0] = 0.
        plot_efficiency(efficiency, lower_error, upper_error, bin_centers, title=plot_title, path=os.path.join(args.out, f"eff_inst_lumi_{path}.png"), xlabel="avg. inst. Luminosity /$(\mu b s)$",ymax=ymax)
        plot_efficiency(efficiency, lower_error, upper_error, bin_centers, title=plot_title, path=os.path.join(args.out, f"eff_inst_lumi_{path}_log.png"), xlabel="avg. inst. Luminosity /$(\mu b s)$", log=True,ymax=ymax)
