
import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
import json

with open("/net/cache/cms/b_tagging/Run3/MuonEG/RunG.json", "r") as json_file:
    fileset = json.load(json_file)

events = NanoEventsFactory.from_root(
    fileset["Run2022G"][0],
    # schemaclass=BaseSchema,
    schemaclass=NanoAODSchema.v6,
).events()
