
import argparse 
import os
import numpy as np
from utils.statistics import getError, getErrorRatio, compute_ratios
from utils.plotting.hists import plot_phi, plot_phi_ratio
from utils.processors import BPixProcessor
from coffea import processor
from coffea.nanoevents import BaseSchema


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true", help="Activate debug settings")
    parser.add_argument("--out", default=f"{os.getenv('HLT_PERFORMANCE_TOOLS_PATH')}/plots", help="Output directory for plots")
    args = parser.parse_args()

    if os.path.isdir(args.out):
        output_path = args.out
    else:
        raise FileNotFoundError(f"Directory {args.out} does not exist! Did you specify one with --out?")

    fileset = {
        "2023 RunC": ["/net/scratch/NiclasEich/BTV/bpix_studies/BTVtuples_Run3/run2023C.root"],
        "2023 RunD": ["/net/scratch/NiclasEich/BTV/bpix_studies/BTVtuples_Run3/run2023D.root"],
        "2023 RunD recovery": ["/net/scratch/NiclasEich/BTV/bpix_studies/BTVtuples_Run3/run2023D_recovery.root"]
        }

    if args.debug:
        maxchunks=10
    else:
        maxchunks=None
    iterative_run = processor.Runner(
        executor=processor.FuturesExecutor(compression=None, workers=32),
        schema=BaseSchema,
        maxchunks=maxchunks,
    )

    # out = iterative_run(
    #     fileset,
    #     treename="EventTupler/DeepJetvars",
    #     processor_instance=EfficiencyProcessor(fileset.keys()),
    # )