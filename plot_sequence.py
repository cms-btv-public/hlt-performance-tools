# -*- coding: utf-8 -*-

"""
Script to plot a trigger efficiency curves from NanoAOD
"""

import os
import numpy
import argparse
import awkward
import coffea.nanoevents

import matplotlib
import matplotlib.pyplot
import mplhep
import scipy.stats

from plot_DQM import getError

matplotlib.use('Agg')
matplotlib.pyplot.style.use(mplhep.style.CMS)

# when using modified NanoAOD
sequences = [
    'hltBTagPFDeepJet0p71Single8Jets30',
    'hltBTagPFDeepCSV0p71Single8Jets30',
    'hltBTagPFDeepJet0p71Double8Jets30',
    'hltBTagPFDeepCSV0p71Double8Jets30',
    'hltBTagPFDeepJet0p71Double6Jets80',
    'hltBTagPFDeepCSV0p71Double6Jets80',
    'hltBTagPFDeepJet1p5Single',
    'hltBTagPFDeepCSV1p5Single',
    'hltBTagPFDeepJet1p59Single',
    'hltBTagPFDeepCSV1p59Single',
    'hltBTagPFDeepJet1p28Single6Jets',
    'hltBTagPFDeepCSV1p28Single6Jets',
    'hltBTagPFDeepJet4p06Single',
    'hltBTagPFDeepCSV4p06Single',
    'hltBTagPFDeepJet2p94Double',
    'hltBTagPFDeepCSV2p94Double',
    'hltBTagPFDeepJet4p5Double',
    'hltBTagPFDeepCSV4p5Double',
    'hltBTagPFDeepJet4p5Triple',
    'hltBTagPFDeepCSV4p5Triple',
    'hltBTagPFDeepJet7p68Double6Jets',
    'hltBTagPFDeepCSV7p68Double6Jets'
]


parser = argparse.ArgumentParser()

parser.add_argument('--path', type=str, default='data/MuonEG_Run2022F_modified.root',
                    help='path to NanoAOD files')

parser.add_argument('--xmin', type=float, default=0.,
                    help='lower variable range')

parser.add_argument('--xmax', type=float, default=1.,
                    help='upper variable range')

parser.add_argument('--bins', type=int, default=20,
                    help='number of bins')


args = parser.parse_args()
print(args)


# load data
print('loading data...')

events = coffea.nanoevents.NanoEventsFactory.from_root(args.path, schemaclass=coffea.nanoevents.NanoAODSchema.v7,).events()

print('loaded!')


# select trigger objects which are Jets (TrigObj_id: 1 = Jet, 6 = FatJet)
events['selTrigObj'] = events.TrigObj[events.TrigObj.id == 1]


#filter events with 0 selected trigger objects
events = events[awkward.num(events.selTrigObj) > 0]


# find closest jet
events['selTrigObj', 'Jet'] = events.selTrigObj.nearest(events.Jet)
events['selTrigObj', 'Jet', 'dr'] = events.selTrigObj.Jet.delta_r(events.selTrigObj)



# decode sequences into filter
for i, sequence in enumerate(sequences):
    print(f'bit {i} is {sequence}')
    events['selTrigObj', sequence] = (events.selTrigObj.filterBits & 1<<i) > 0




for sequence in sequences:
    if not 'DeepJet' in sequence:
        continue

    print(f'plotting {sequence}...')
    deepjetsequence = sequence
    deepcsvsequence = sequence.replace('DeepJet', 'DeepCSV')

    fig = matplotlib.pyplot.figure(figsize=(10, 10))
    plot = fig.add_subplot()

    mplhep.cms.label(loc=0, ax=plot, data=True, paper=False, rlabel='$13.6\,$TeV')


    bins = numpy.linspace(args.xmin, args.xmax, args.bins+1)


    def getBTags(sequence):
        tmp = events.selTrigObj.Jet.btagDeepFlavB[events['selTrigObj', sequence]]
        return awkward.flatten(tmp).to_numpy()

    deepjethist = numpy.histogram(getBTags(deepjetsequence), bins=bins)[0]
    deepcsvhist = numpy.histogram(getBTags(deepcsvsequence), bins=bins)[0]

    mplhep.histplot([(deepcsvhist, bins), (deepjethist, bins)],
                    yerr=[numpy.sqrt(deepcsvhist), numpy.sqrt(deepjethist)],
                    ax=plot,
                    histtype='errorbar',
                    density=True,
                    label=[deepcsvsequence, deepjetsequence]
                    )



    plot.legend(loc=2)
    plot.set_xlabel('Offline btagDeepFlavB (matched jet)', x=1.0, ha='right')
    plot.set_xlim(args.xmin, args.xmax)
    #plot.set_ylim(0, 1.2)
    plot.set_ylabel('number of jets (normalized)', verticalalignment='bottom', y=1.0,ha='right')


    fig.tight_layout()
    os.makedirs('plots', exist_ok = True)
    fig.savefig(f'plots/{sequence}.pdf', dpi=300)
    #matplotlib.pyplot.show()
    matplotlib.pyplot.close()
