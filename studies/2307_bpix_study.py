import argparse
import os
import numpy as np
from utils.plotting.hists import plot_phi, plot_phi_ratio
from utils.processors import BPixProcessor
from coffea import processor
from coffea.nanoevents import BaseSchema


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true", help="Activate debug settings")
    parser.add_argument(
        "--out",
        default=f"{os.getenv('HLT_PERFORMANCE_TOOLS_PATH')}/plots",
        help="Output directory for plots",
    )
    args = parser.parse_args()

    if os.path.isdir(args.out):
        output_path = args.out
    else:
        raise FileNotFoundError(
            f"Directory {args.out} does not exist! Did you specify one with --out?"
        )

    fileset = {
        "2023 RunC": ["/net/scratch/NiclasEich/BTV/bpix_studies/BTVtuples_Run3/run2023C.root"],
        "2023 RunD": ["/net/scratch/NiclasEich/BTV/bpix_studies/BTVtuples_Run3/run2023D.root"],
        "2023 RunD recovery": [
            "/net/scratch/NiclasEich/BTV/bpix_studies/BTVtuples_Run3/run2023D_recovery.root"
        ],
    }

    if args.debug:
        maxchunks = 10
    else:
        maxchunks = None
    iterative_run = processor.Runner(
        executor=processor.FuturesExecutor(compression=None, workers=32),
        schema=BaseSchema,
        maxchunks=maxchunks,
    )

    out = iterative_run(
        fileset,
        treename="EventTupler/DeepJetvars",
        processor_instance=BPixProcessor(fileset.keys()),
    )

    textstr = "$|\eta| < 2.5$" + "\n" + r"$30\mathrm{GeV} < p_T < 1000\mathrm{GeV}$"
    plot_phi(
        out["phi"],
        os.path.join(output_path, "phi.png"),
        textstr=textstr,
        xlabel="Jet $\phi$",
        ylabel="# Jets (normalized)",
        ylim=(0.05, 0.08),
        normalize=True,
    )
    textstr = (
        r"$|\eta| < 2.5$"
        + "\n"
        + r"$30\mathrm{GeV} < p_T < 1000\mathrm{GeV}$"
        + "\n"
        + "b-tag > 0.4"
    )
    plot_phi(
        out["phi_selected"],
        os.path.join(output_path, "phi_selected.png"),
        textstr=textstr,
        xlabel="Jet $\phi$",
        ylabel="# Jets selected (normalized)",
        ylim=(0.05, 0.08),
        normalize=True,
    )

    textstr = (
        "$|\eta| < 2.5$"
        + "\n"
        + r"$30\mathrm{GeV} < p_T < 1000\mathrm{GeV}$"
        + "\n"
        + "b-tag > 0.4"
    )
    plot_phi_ratio(
        out["phi_selected"],
        out["phi"],
        os.path.join(output_path, "phi_divided.png"),
        textstr=textstr,
        xlabel="Jet $\phi$",
        ylabel="Passed/Total",
        normalize=True,
    )

    textstr = (
        "Tag and Probe"
        + "\n"
        + r"dr$(j_{probe},j_{tag}) < 1.4$"
        + "\n"
        + r"dr$(j_{tag},\mu) < 0.4$"
        + "\n"
        + "btag = 0.3"
    )
    plot_phi_ratio(
        out["tnp_passed"],
        out["tnp_total"],
        os.path.join(output_path, "tnp.png"),
        textstr=textstr,
        xlabel="Jet$_{probe}\ \phi$",
        ylabel="TnP Efficiency",
        ylim=(0.0, 0.8),
    )
